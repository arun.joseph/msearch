const movies = require('../data/dataset.json');

module.exports = function searchMovies(input) {
    if (input === null) {
        return {search: input, movies:[], tcount: 0};
    }
    else if (input.length === 0) {
        return {search: input, movies:[], tcount: 0};
    }
    let results = [];
    let totCount = 0;
    let regExPattern = new RegExp(input, 'i');
    movies.forEach(function(movie) {
        Object.keys(movie).forEach(function(key) {
            if (typeof movie[key] === 'number') return;
            if (typeof movie[key] === 'object') {
                let status = movie[key].some((arrItem) => {
                    return arrItem.match(regExPattern)
                });
                totCount = totCount + movie[key].filter( (item) => {
                return item.match(regExPattern)
                }).length;
                if(status) {
                    if (results.indexOf(movie) === -1) {
                        results.push(movie);
                    }
                }
            }
            else if (movie[key].match(regExPattern)) {
                totCount = totCount + 1;
                if (results.indexOf(movie) === -1) {
                    results.push(movie);
                }
            }
        });
    });

    return {search: input, movies:results, tcount: totCount};
}