let Promise = require('bluebird');
module.exports = function getUserInput() {
    let stdin = process.stdin;
    let userInput = []

    stdin.setEncoding('utf-8');
    console.log("Input:");
    return new Promise((resolve, reject) => {
        process.stdin.once('data', (data) => {
            userInput.push(data.toString().trim())
            resolve(userInput);
        });
    });
}