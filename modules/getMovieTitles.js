module.exports = function getMovieTitles(moviesCollection) {
    let titles = "";
    moviesCollection.forEach((movie) => {
        movie.movies.forEach((item) => {
            if (titles.length === 0) {
                titles = item["title"];
            }    
            else {
                titles = titles + ", " + item["title"];
            }
        });
    });
    return titles;
}