const searchMovies = require('./modules/movieSearch');
const getUserInp = require('./modules/getUserInput');
const getMovieTitles = require('./modules/getMovieTitles');

getUserInp()
    .then((tests) => {
        let output = tests.map(searchMovies);
        let titles = getMovieTitles(output);
        output.forEach((movie) => {
            if (movie.tcount === 0) {
                console.log("\nSearch did not return any results.");
            }
            else {
                console.log("\nSearch Term: " + movie.search);
                console.log("Movies: " + titles);
                console.log("Count: " + movie.tcount);
            }
            process.exit();
        });
    });