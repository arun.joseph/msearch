const getMovieTitles = require('../modules/getMovieTitles');

describe('test getMovieTitles module', () => {
    const testData = require('./testdata/getMovieTitleSData.json');
    
    it('should return list of movie titles in a string delimited with comma when multiple movies are present in array', () => {
        
        let result = getMovieTitles(testData);
        testData[0].movies.forEach(movie => {
            expect(result).toContain(movie.title);                
        });
        expect(result).toContain(', ');
        expect(result.split(', ').length).toBe(3);
    });

    it('should return single movie title without commas as a string when single movie is present in array', () => {
        let movie = testData[0].movies[0];
        let singleInput = [{"movies": [movie] }];
        let result = getMovieTitles(singleInput);
        expect(result).toContain(singleInput[0].movies[0].title);
        expect(result).not.toContain(', ');
        expect(result.split(', ').length).toBe(1);
    });

    it('should return empty string when no movie is present in the array', () => {
        let emptyInput = [{"movies": [] }];
        let result = getMovieTitles(emptyInput);
        expect(result).toBe('');
    });
});