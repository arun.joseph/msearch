const getUserInp = require('../modules/getUserInput');

describe('test get user input module', () => {
    let stdin;
    beforeEach(() => {
        stdin = require('mock-stdin').stdin();
    });
    it('should return the string input as an array', () => {
        process.nextTick(() => {
            stdin.send('test');
        });
        return getUserInp()
            .then((result) => {
                expect(result).toEqual(['test']);
            });
    });
    it('should return the empty string input as an array with empty string', () => {
        process.nextTick(() => {
            stdin.send('\n');
        });
        return getUserInp()
            .then((result) => {
                expect(result).toEqual(['']);
            });
    });
    it('should return the string with a single space input as an array with empty string', () => {
        process.nextTick(() => {
            stdin.send(' ');
        });
        return getUserInp()
            .then((result) => {
                expect(result).toEqual(['']);
            });
    });
});