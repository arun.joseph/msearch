const searchMovie = require('../modules/movieSearch');

describe('test movie search module', () => {
    let inputs = require('./testdata/movieSearchData.json');
    
    it('should return correct number of occurance of the search pattern', () => {
        inputs.forEach(input => {
            let result = searchMovie(input.search);
            expect(result.tcount).toBe(input.expected);
        });       
    });
    it('should return correct names of movies containing the search pattern', () => {
        inputs.forEach(input => {
            let result = searchMovie(input.search);
            result.movies.forEach( movie => {
                expect(input.movies).toContain(movie.title);
            });
            expect(result.movies.length).toBe(input.movies.length);
        });
    });
    it('should return correct count of movies containing the search pattern', () => {
        inputs.forEach(input => {
            let result = searchMovie(input.search);
            expect(result.movies.length).toBe(input.movies.length);
        });
    });
    it('should return the correct search pattern send as the input', () => {
        inputs.forEach(input => {
            let result = searchMovie(input.search);
            expect(result.search).toBe(input.search);
        });
    });
    it('should return the 0 count when search string is null', () => {
        let result = searchMovie(null);
        expect(result.tcount).toBe(0);
    });
    it('should return the 0 count when search string is empty string', () => {
        let result = searchMovie('');
        expect(result.tcount).toBe(0);
    });
    it('should return empty movie array when search string is null', () => {
        let result = searchMovie(null);
        expect(result.movies).toEqual([]);
    });
    it('should return empty movie array when search string is empty string', () => {
        let result = searchMovie('');
        expect(result.movies).toEqual([]);
    });
    it('should return Search Term as null string when search string is null', () => {
        let result = searchMovie(null);
        expect(result.search).toEqual(null);
    });
    it('should return empty movie array when search string is empty string', () => {
        let result = searchMovie('');
        expect(result.search).toEqual('');
    });
    it('should return 0 result count when search pattern returns no results', () => {
        let result = searchMovie('Invalid Input');
        expect(result.tcount).toBe(0);
    });
    it('should return empty movie array when search pattern returns no results', () => {
        let result = searchMovie('Invalid Input');
        expect(result.movies).toEqual([]);
    });
});