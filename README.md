# mSearch

The script allows the user to search for a string patter from a movies collection in a JSON file and return the movies that match the search pattern and the count of the occurrence of the search pattern in the JSON.

How to start
1) Install nodeJS
2) Download/clone the repository to a local folder
3) Open terminal and navigate to the folder with package.json
4) Run the command $npm install
5) Run the command npm test to verify the installation
6) Press q in keyboard to exit test mode
7) Run node index.js to run the program

How to use
1) The movies data is located in ./data/dataset.json. Data can be updated as required.
2) The program asks for a user input where the user can enter a search string.
3) The output provides the search string used, titles of movies which match the search string and the number of occurences of the string.